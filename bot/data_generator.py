import uuid
from typing import List
from dataclasses import dataclass
from random import choice, randint
from bot_config import (
    NUMBER_OF_USERS,
    MAX_LIKES_PER_USER,
    MAX_POSTS_PER_USER,
)
from mock_data import *


@dataclass
class User:
    email: str
    password: str
    first_name: str or None
    last_name: str or None


@dataclass
class Post:
    title: str
    text: str


@dataclass
class TestCase:
    user: User
    posts: List[Post]
    likes: int


class DataGenerator:

    def get_test_cases(self):

        generated = []
        for _ in range(NUMBER_OF_USERS):
            user = User(
                email=self.generate_email(),
                password=self.generate_password(),
                first_name=self.generate_first_name(),
                last_name=self.generate_last_name()
            )
            posts = []
            for _ in range(randint(1, MAX_POSTS_PER_USER)):
                posts.append(
                    Post(
                        title=self.generate_title(),
                        text=self.generate_text()
                    )
                )
            generated.append(
                TestCase(
                    user=user,
                    posts=posts,
                    likes=MAX_LIKES_PER_USER
                )
            )
        return generated

    @staticmethod
    def generate_email():

        part_1 = choice(names_1)
        part_2 = choice(names_1)
        part_3 = choice(names_2)
        part_4 = choice(domains)

        return f'{part_1}.{part_2}@{part_3}.{part_4}'

    @staticmethod
    def generate_password():
        return str(uuid.uuid1())

    @staticmethod
    def generate_first_name():
        return choice(names_1)

    @staticmethod
    def generate_last_name():
        return choice(names_2)

    @staticmethod
    def generate_title():
        return choice(titles)

    @staticmethod
    def generate_text():
        text = ''
        for _ in range(randint(1,7)):
            text += ' ' + choice(sentences)
        return text
