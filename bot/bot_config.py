"""Bot settings"""
NUMBER_OF_USERS = 10
MAX_POSTS_PER_USER = 10
MAX_LIKES_PER_USER = 5


CREATE_USER_URL = 'http://127.0.0.1:8000/auth/users/'
LOGIN_USER_URL = 'http://127.0.0.1:8000/auth/login/'
CREATE_POST_URL = 'http://127.0.0.1:8000/api/posts/'
LIKE_POST_URL = """http://127.0.0.1:8000/api/like/{post_id}/"""
