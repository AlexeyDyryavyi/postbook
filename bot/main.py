import aiohttp
import asyncio
import logging
from random import choices, randint
from bot_config import (
    CREATE_USER_URL,
    CREATE_POST_URL,
    LOGIN_USER_URL,
    LIKE_POST_URL,
)
from data_generator import DataGenerator

logger = logging.getLogger("Bot")
logging.basicConfig(level=logging.DEBUG)


class BotService:
    """Bot service for test"""
    PROGRESSIVE_SCALE = [1, 2, 3, 5, 7, 10, 15, 30, 45, 60]

    __posts_ids = []
    __users_ids = []

    def __init__(self, case):
        self._logger = logger
        self.user = case.user
        self.posts = case.posts
        self.likes = case.likes

    @staticmethod
    async def _sleep(seconds: int):
        """Simple async sleep with random gap.
        """
        await asyncio.sleep(seconds + randint(1, 10) / 10)

    async def create_user(self):
        user = await self._process_request(
            method='post',
            url=CREATE_USER_URL,
            headers={},
            json={
                'email': self.user.email,
                'password': self.user.password,
                'first_name': self.user.first_name,
                'last_name': self.user.last_name,
            }
        )
        if user:
            self.__users_ids.append(user['id'])
            return user

    async def get_token(self):
        result = await self._process_request(
            method='post',
            url=LOGIN_USER_URL,
            headers={},
            json={
                'email': self.user.email,
                'password': self.user.password,
            }
        )
        if result:
            return result['access']

    async def create_posts(self):
        tasks = []
        token = await self.get_token()
        if token:
            for post in self.posts:
                tasks.append(
                    self._process_request(
                        method='post',
                        url=CREATE_POST_URL,
                        headers={
                            'Authorization': f'Bearer {token}'
                        },
                        json={
                            'title': post.title,
                            'text': post.text,
                        }
                    )
                )
        results = await asyncio.gather(*tasks)
        if results:
            self.__posts_ids += [x['pk'] for x in results]
            return results

    async def perform_likes(self):
        token = await self.get_token()
        post_list = choices(self.__posts_ids, k=randint(1, self.likes))
        tasks = []
        for post_id in post_list:
            tasks.append(
                self._process_request(
                    method='post',
                    url=LIKE_POST_URL.format(post_id=post_id),
                    headers={
                        'Authorization': f'Bearer {token}'
                    },
                    json={}
                )
            )
        await asyncio.gather(*tasks)

    async def apply_mock_for_datetime(self):
        pass

    async def _process_request(self, method,  url: str, headers: dict, json):
        """
        Process  request
        :param method: get or post
        :param url: url to API
        :param headers: Authorization
        :return: API request response as dict
        """
        self._logger.info(f'Processing {method} request to {url}')
        # ~ proceed request ~
        async with aiohttp.ClientSession() as session:
            for timeout in self.PROGRESSIVE_SCALE + [None]:
                if method == "get":
                    request = session.get
                elif method == "post":
                    request = session.post
                try:
                    async with request(
                            url=url,
                            headers=headers,
                            json=json
                    ) as response:
                        self._logger.info(f'API status request: {str(response)}')
                        if response.status // 100 == 2:
                            self._logger.info('Successful.')
                            return await response.json()
                        # 429 and 5xx we should repeat in progress scale:
                        elif (response.status == 429 or response.status // 100 == 5) and timeout is not None:
                            self._logger.error(response.status, timeout)
                            await self._sleep(timeout)
                        elif response.status in (400, 401, 404):
                            return None
                        # raising error in all other cases:
                        else:
                            raise aiohttp.ClientError(await response.text())
                except Exception as e:
                    if isinstance(e, aiohttp.ClientConnectionError):
                        await self._sleep(timeout)
                    else:
                        self._logger.error(
                            f"Error Executing request: {e}", self
                        )
                        return None


async def main():
    logger.info('Bot starts')
    cases = DataGenerator().get_test_cases()
    create_users = [BotService(x).create_user() for x in cases]
    create_posts = [BotService(x).create_posts() for x in cases]
    perform_likes = [BotService(x).perform_likes() for x in cases]

    logger.info('Creating users')
    users = await asyncio.gather(*create_users)
    if users:
        logger.info('Users were created')
        logger.info('Creating posts')
        posts = await asyncio.gather(*create_posts)
        if posts:
            logger.info('Posts were created')
            logger.info('Perform likes posts')
            await asyncio.gather(*perform_likes)
    logger.info('Bot stops')

if __name__ == '__main__':
    asyncio.run(main())
