# postbook

`api` — backend application

`build` — build scripts, docker/docker-compose

`bot` — bot scripts

### How to run

`docker-compose -f build/docker-compose-dev.yml up`

will run only PostgreSQL

`docker-compose -f build/docker-compose-api.yml up`

will run PostgreSQL and API (backend application)

### How to test

`bot/bot_config.py` — bot configuration

`bot/main.py` — bot main script
