from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = "postbook.apps.account"
