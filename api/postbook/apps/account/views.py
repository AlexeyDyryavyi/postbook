from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import viewsets
from django.db.models import Max
from rest_framework.response import Response

from postbook.apps.account.serializers import LoginSerializer
from postbook.apps.account.models import User


class LoginView(TokenObtainPairView):
    serializer_class = LoginSerializer


class StatsView(viewsets.ViewSet):
    """
       A simple ViewSet to get user activity analytics
       """
    def retrieve(self, request, pk=None):
        last_login = User.objects.aggregate(m=Max('last_login')).get('m')
        last_action = User.objects.aggregate(m=Max('last_action')).get('m')
        result = {
            "last_login": last_login,
            "last_action": last_action,
        }
        return Response(result)
