from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = "postbook.apps.common"
