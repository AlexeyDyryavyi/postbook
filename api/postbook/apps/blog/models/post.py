from django.db import models

from postbook.apps.account.models import User
from postbook.apps.common.models import CoreModel


class Like(CoreModel):
    author = models.ForeignKey(User, related_name='like_author',
                               on_delete=models.CASCADE, blank=True, null=True)


class Post(CoreModel):
    title = models.CharField(max_length=100)
    text = models.TextField()
    author = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE, blank=True, null=True)
    likes = models.ManyToManyField(Like, related_name='likes', blank=True, )
