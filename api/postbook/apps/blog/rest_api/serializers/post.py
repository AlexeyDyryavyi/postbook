from rest_framework import serializers

from postbook.apps.account.serializers import UserProfileSerializer
from postbook.apps.blog.models.post import Post, Like


class LikeSerializer(serializers.ModelSerializer):
    author = UserProfileSerializer(required=False, read_only=True)

    class Meta:
        model = Like
        fields = ('pk', 'author',)


class PostSerializer(serializers.ModelSerializer):
    author = UserProfileSerializer(required=False, read_only=True)
    likes = LikeSerializer(required=False, many=True)
    serializers.ImageField(use_url=True, required=False, allow_null=True)

    class Meta:
        model = Post
        fields = ('pk', 'title', 'text', 'author', 'likes',)
