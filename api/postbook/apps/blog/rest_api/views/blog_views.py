from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Count, DateTimeField
from django.db.models.functions import Trunc
from django.utils import timezone
from datetime import datetime

from postbook.apps.account.models import User
from postbook.apps.blog.models.post import Post, Like
from postbook.apps.blog.rest_api.serializers.post import PostSerializer, LikeSerializer


class PostListCreateAPIView(ListCreateAPIView):
    """
    API view to retrieve list of posts or create new
    """
    serializer_class = PostSerializer
    queryset = Post.objects.active()

    def perform_create(self, serializer):
        user = self.request.user
        user.last_action = timezone.now()
        user.save(update_fields=['last_action'])
        serializer.save(author=user)


class PostDetailsAPIView(RetrieveUpdateDestroyAPIView):
    """
    API view to retrieve, update or delete post
    """
    serializer_class = PostSerializer
    queryset = Post.objects.active()

    def get(self, request, *args, **kwargs):
        user = self.request.user
        user.last_action = timezone.now()
        user.save(update_fields=['last_action'])
        return self.retrieve(request, *args, **kwargs)


class LikeAPIView(PostListCreateAPIView):
    """
    API view to update post
    """
    serializer_class = LikeSerializer

    def perform_create(self, serializer):
        post = self.get_object()
        user = self.request.user
        user.last_action = timezone.now()
        user.save(update_fields=['last_action'])
        if not self.queryset.filter(likes__author=user, pk=post.pk):
            serializer.is_valid(raise_exception=True)
            serializer.save(author=user)
            post.likes.add(serializer.instance)
            post.save()
        else:
            like = Like.objects.filter(author__id=user.pk, likes__id=post.pk)
            like.delete()


class AnalyticsAPIView(viewsets.ViewSet):
    """
    A simple ViewSet to get analytics
    """
    def retrieve(self, request, pk=None):

        date_from = datetime.strptime(self.request.query_params['date_from'], "%Y-%m-%d")
        date_to = datetime.strptime(self.request.query_params['date_to'], "%Y-%m-%d")
        current = timezone.get_current_timezone()
        l_from, l_to = current.localize(date_from), current.localize(date_to)

        queryset_1 = Like.objects.filter(created__range=(l_from, l_to)) \
            .annotate(date=Trunc('created', 'day', output_field=DateTimeField())) \
            .values('date') \
            .annotate(likes_count=Count('id')) \
            .order_by('-date')

        queryset_2 = Post.objects.filter(created__range=(l_from, l_to)) \
            .annotate(date=Trunc('created', 'day', output_field=DateTimeField())) \
            .values('date') \
            .annotate(new_posts_count=Count('id')) \
            .order_by('-date')

        queryset_3 = User.objects.filter(created__range=(l_from, l_to)) \
            .annotate(date=Trunc('created', 'day', output_field=DateTimeField())) \
            .values('date') \
            .annotate(new_users_count=Count('id')) \
            .order_by('-date')

        result = [{str(x['date']): {'likes_count': x['likes_count']}} for x in queryset_1] + \
                 [{str(x['date']): {'new_posts_count': x['new_posts_count']}} for x in queryset_2] + \
                 [{str(x['date']): {'new_users_count': x['new_users_count']}} for x in queryset_3]

        union = {}
        for item in result:
            for key, data in item.items():
                if key not in union.keys():
                    union[key] = {}
                union[key].update(data)

        user = self.request.user
        user.last_action = timezone.now()
        user.save(update_fields=['last_action'])
        return Response(union)
